import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
   public Server(){
      try{
            ServerSocket server = null;
            Socket client;
            
            server = new ServerSocket(4444);    // ������� ���� 1234
            client = server.accept();                    //���� ����������� �������
          
            System.out.println("Got client");  //���������, ��� ������ ���������
            
            PrintWriter out = null;
            BufferedReader in = null;
            
            in = new BufferedReader(new InputStreamReader(client.getInputStream())); 
            out = new PrintWriter(client.getOutputStream(),true);
           
            String input;

            while ((input = in.readLine()) != null) {
                if (input.equalsIgnoreCase("quit")) {
                    break;
                }
                out.println("Server> " + input);  //�������� �������
                System.out.println(input);
            }          
            out.close();                              
            in.close();
            client.close();
            server.close();
            
            System.out.println("Server closed");
        } catch(Exception e){
            System.out.println(e.getMessage());
        }  
 }
 
 public static void main(String[] args) {
          new Server();                                          //��������� 
   }
}