import java.awt.Frame;
import java.awt.Robot; 
import java.awt.event.KeyEvent; 
import java.io.BufferedReader; 
import java.io.InputStreamReader; 
import java.io.PrintWriter; 
import java.net.ServerSocket; 
import java.net.Socket;

public class AndroidServer implements Runnable {

    public PrintWriter out;
    public BufferedReader in;
    public ServerSocket server;
    public Socket client;
    public String input;
    public int flag = 0;
    public int flagMin = 0;
    public Robot kPress;

    public void run() { 
       
        try {
            server = null; 
            kPress = new Robot(); 
            
            server = new ServerSocket(4444);   
            client = server.accept();           
            flag = 1; 
            
            out = null; 
            in = null;
            
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));  
            out = new PrintWriter(client.getOutputStream(), true);

            /*
             * ���������� ��������� ��������, ������� ������ 
             */
            
            while ((input = in.readLine()) != null) { 
                out.println("Online");  
                System.out.println(input);
                if(Integer.parseInt(input.trim()) == 3 && flagMin == 0) { 
                    //liveRecognitionViewFrame.getFrame().setState(Frame.ICONIFIED);
                    flagMin = 1;
                } else if (Integer.parseInt(input.trim()) == 3 && flagMin == 1) {
                    //liveRecognitionViewFrame.getFrame().setState(Frame.NORMAL);
                    flagMin = 0;
                } else if (Integer.parseInt(input.trim()) == 1) {
                    kPress.keyPress(KeyEvent.VK_LEFT);
                } else if (Integer.parseInt(input.trim()) == 2) {
                    kPress.keyPress(KeyEvent.VK_RIGHT);
                }
            }          
            out.close();                           
            in.close();
            client.close();
            server.close();
            
        } catch(Exception e) { 
            System.out.println(e.getMessage());
        }
    }
}