package ru.androidclient;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.content.Context;

public class MainActivity extends Activity implements OnClickListener {
	private Socket clientSocket;
	public Toast toast;
	
	private Button bLeft, bRight, bEx, bSub; 
	private EditText eIp, ePort;
	private String ip, command, server;
	private int port;
	private BufferedReader in, inu;
	private PrintWriter out;
	private TextView State;
	public static Context MainContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// �������� ��������
        MainContext = getApplicationContext();
		
		bLeft = (Button)findViewById(R.id.button1);
		bRight = (Button)findViewById(R.id.button2);
		bEx = (Button)findViewById(R.id.button3);
		bSub = (Button)findViewById(R.id.button4);
		bLeft.setOnClickListener(this);
		bRight.setOnClickListener(this);
		bEx.setOnClickListener(this);
		bSub.setOnClickListener(this);
		
		eIp = (EditText)findViewById(R.id.editText1);
		ePort = (EditText)findViewById(R.id.editText2);
		
		State = (TextView)findViewById(R.id.textView2);
		
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.button1:
			out.println("1");
			break;
			
        case R.id.button2:
        	out.println("2");
        	break;
        	
        case R.id.button3:
        	out.println("3");

        	break; 
        	
        case R.id.button4:
        	if(eIp.getText().toString().equals("")) {
        		toast = Toast.makeText(getApplicationContext(), "������ ���� IP", Toast.LENGTH_SHORT); 
        		toast.show();
        		return;
        	} else if (ePort.getText().toString().equals("")) {
        		toast = Toast.makeText(getApplicationContext(), "������ ���� Port", Toast.LENGTH_SHORT); 
        		toast.show();
        		return;
        	} else {
        		ip = eIp.getText().toString().trim();
    			port = Integer.parseInt(ePort.getText().toString().trim());
        	}
			
        	ClientAsyncTask mAT = new ClientAsyncTask();
	    	mAT.execute();
	    	
			// ������������� �� �������
	    	final Handler handler = new Handler();
	        final Runnable showInfo = new Runnable() {
	          @Override
	          public void run() {
	        	  Toast.makeText( MainContext, "Call 4" , Toast.LENGTH_SHORT).show();
	        	  out.println("4");
	        	  }
	          };
	          
	          Thread t = new Thread(new Runnable() {
	              public void run() {
	                  try {
	                      while (true) {
	                      	TimeUnit.MILLISECONDS.sleep(10000);
	                      	handler.post(showInfo);
	                      	}
	                      } catch (InterruptedException e) {
	                      	e.printStackTrace();
	                      	}
	                  }
	              });
	          t.start(); 
	    	
	    	bSub.setEnabled(false);

        	break;
			
		}
  
	}
	
	private class ClientAsyncTask extends AsyncTask<Void, Integer, Void> {
		
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
		
		@Override
		protected Void doInBackground(Void... arg0) {
        	if(Looper.myLooper() == null) {
        		Looper.prepare(); // ��� ����� ���������� ������, ��� ��� ���������� �� � �������� ����������� ������
        	    }
        	
			try {
				new Client(ip, port);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}
		
		@Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            State.setText(server);
        }

	}
	
	private class Client {
		Client(String ip, int port) throws UnknownHostException, IOException {
			Socket clientSocket = null;
            clientSocket = new Socket(ip, port);
            
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            inu = new BufferedReader(new InputStreamReader(System.in));
            
            server = in.readLine();
            
		}
		
	}
}